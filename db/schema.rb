# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_02_24_122941) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "imports", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "stores", force: :cascade do |t|
    t.string "owner"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.decimal "current_balance", precision: 10, scale: 2, default: "0.0"
  end

  create_table "transactions", force: :cascade do |t|
    t.integer "transaction_type"
    t.string "card_number"
    t.string "hour"
    t.string "date"
    t.bigint "store_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "import_id"
    t.string "document"
    t.decimal "amount", precision: 10, scale: 2
    t.index ["import_id"], name: "index_transactions_on_import_id"
    t.index ["store_id"], name: "index_transactions_on_store_id"
  end

  add_foreign_key "transactions", "imports"
  add_foreign_key "transactions", "stores"
end
