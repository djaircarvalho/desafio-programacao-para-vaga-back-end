class AddImportAndDocumentToTransaction < ActiveRecord::Migration[5.2]
  def change
    add_reference :transactions, :import, foreign_key: true
    add_column :transactions, :document, :string
  end
end
