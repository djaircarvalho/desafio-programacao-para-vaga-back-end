class CreateTransactions < ActiveRecord::Migration[5.2]
  def change
    create_table :transactions do |t|
      t.integer :type
      t.string :card_number
      t.string :hour
      t.string :date
      t.string :amount, :decimal, precision: 10, scale: 2
      t.references :store, foreign_key: true

      t.timestamps
    end
  end
end
