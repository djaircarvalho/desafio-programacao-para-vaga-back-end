class FixAmountType < ActiveRecord::Migration[5.2]
  def change
    remove_column :transactions, :amount
    change_table :transactions do |t|
      t.decimal :amount, precision: 10, scale: 2
    end
    remove_column :transactions, :decimal
  end
end
