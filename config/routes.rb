Rails.application.routes.draw do
  resources :stores, only: [:index, :show]
  resources :imports, only: [:index, :new, :create]
  root 'imports#index'
end
