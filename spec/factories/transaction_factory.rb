FactoryBot.define do
  factory :transaction do
    hour { "12344" }
    date { "12344" }
    card_number { "12344**123" }
    transaction_type { 1 }
    amount { 10 }
    document { '101231232' }
    
    trait :debit do
      transaction_type { 2 }
      amount { 2 }
    end
  end
end