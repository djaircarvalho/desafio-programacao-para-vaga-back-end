FactoryBot.define do
  factory :store do
    name { "Padaria do Zé" }
    owner { "Padaria do Zé" }
  end
end