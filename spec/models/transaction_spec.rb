require 'rails_helper'

RSpec.describe Transaction, type: :model do
  describe 'associations' do
    it { should belong_to(:store) }
    it { should belong_to(:import) }
  end

  describe 'validations' do
    it { should validate_presence_of(:hour) }
    it { should validate_presence_of(:date) }
    it { should validate_presence_of(:card_number) }
    it { should validate_presence_of(:transaction_type) }
    it { should validate_presence_of(:amount) }
    it { should validate_presence_of(:document) }
  end
end
