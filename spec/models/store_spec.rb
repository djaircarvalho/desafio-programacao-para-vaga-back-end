require 'rails_helper'

RSpec.describe Store, type: :model do
  it { should validate_presence_of(:owner) }
  it { should validate_presence_of(:name) }
end
