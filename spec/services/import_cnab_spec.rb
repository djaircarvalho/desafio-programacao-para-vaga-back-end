require 'rails_helper'

describe ImportCNAB do
  let(:file_path) { "spec/fixtures/cnab/one-line-CNAB.txt" }
  let(:parser) { CNABParser.new(file_path) }
  let(:subject) { described_class.new(parser).call }

  it 'creates an import' do
    expect{subject}.to change{Import.count}.by(1)
  end
end