require 'rails_helper'

describe UpdateStoreCredits do
  let(:store) { create(:store) }
  let(:import) do
    create(:import) do |import|
      credit = build(:transaction)
      credit.store = store
      debit = build(:transaction, :debit)
      debit.store = store
      import.transactions << credit
      import.transactions << debit
    end
  end
  
  let(:subject) { described_class.new(store.id, import.id).call }

  it 'updates the store balance' do
    subject
    store.reload
    expect(store.current_balance).to eq(8.0)
  end
end