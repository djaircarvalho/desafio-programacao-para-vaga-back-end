require 'rails_helper'

RSpec.describe "Imports", type: :request do

  describe "GET /" do
    it "returns http success" do
      get "/imports"
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET /new" do
    it "returns http success" do
      get "/imports/new"
      expect(response).to have_http_status(:success)
    end
  end

  describe "POST /create" do
    context 'success' do
      let(:subject) { post "/imports",  params: { file: Rack::Test::UploadedFile.new('spec/fixtures/cnab/one-line-CNAB.txt', 'text/plain') }}
        
      it "returns http success" do
        expect(subject).to redirect_to(imports_url)
      end

      it "creates a import" do
        expect{ subject }.to change{ Import.count }.by(1)
      end
      
      it "creates its transactions" do
        expect{ subject }.to change{ Transaction.count }.by(1)
      end
    end
  end
end
