require 'rails_helper' 

describe CNABImportWorker, type: :worker do
  
  it 'jobs are enqueued' do
    described_class.perform_async(1, 2)
    
    expect(described_class.queue).to eq('default')
   
    expect do
      described_class.perform_async(1,2)
    end.to change(described_class.jobs, :size).by(1)
  end

  it 'calls the UpdateStoreCreditsService' do
    allow(Store).to receive(:find)
    allow(Import).to receive(:find)
    
    expect_any_instance_of(UpdateStoreCredits).to receive(:call)
    described_class.new.perform(1, 2)
  end
end