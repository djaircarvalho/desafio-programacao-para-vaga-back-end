require 'rails_helper'

describe CNAB::LineParser do
let(:subject) { described_class.new('5201903010000013200556418150633123****7687145607MARIA JOSEFINALOJA DO Ó - MATRIZ').process }

  it 'parses the transaction type' do
    expect(subject.type).to eq(5)
  end

  it 'parses the transaction date' do
    expect(subject.date).to eq('20190301')
  end

  it 'parses the transaction amount' do
    expect(subject.amount).to eq(132.0)
  end
  
  it 'parses the transaction document' do
    expect(subject.document).to eq('55641815063')
  end

  it 'parses the transaction card_number' do
    expect(subject.card_number).to eq('3123****7687')
  end

  it 'parses the transaction hour' do
    expect(subject.hour).to eq('145607')
  end

  it 'parses the transaction store owner' do
    expect(subject.store_owner).to eq('MARIA JOSEFINA')
  end

  it 'parses the transaction store name' do
    expect(subject.store_name).to eq('LOJA DO Ó - MATRIZ')
  end

end