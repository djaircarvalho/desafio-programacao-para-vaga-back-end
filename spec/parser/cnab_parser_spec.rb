require 'rails_helper'

describe CNABParser do
  let(:file_path) { "spec/fixtures/cnab/CNAB.txt" }
  let(:subject) { described_class.new(file_path).parse }

  it 'process every line' do
    expect(subject.size).to eq(21)
  end
end