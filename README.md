# README

Uso sidekiq pro processamento em background. Pode instalar o redis na máquina ou via docker: `docker run  --rm -p 6379:6379 redis`

Passos para rodar o app: 
1. bundle
2: rails db:setup
3. rails s
4. bundle exec sidekiq
5: acessar localhost:3000
