
TransactionOperation = Struct.new(:id, :description, :type, :operator)

class UpdateStoreCredits::TransactionOperationConverter
  class << self
    KNOWN_OPERATIONS = [
      TransactionOperation.new(1,'Débito',	'Entrada', '+'),
      TransactionOperation.new(2,'Boleto',	'Saída', '-'),
      TransactionOperation.new(3,'Financiamento',	'Saída', '-'),
      TransactionOperation.new(4,'Crédito',	'Entrada', '+'),
      TransactionOperation.new(5,'Recebimento Empréstimo',	'Entrada', '+'),
      TransactionOperation.new(6,'Vendas',	'Entrada', '+'),
      TransactionOperation.new(7,'Recebimento TED',	'Entrada', '+'),
      TransactionOperation.new(8,'Recebimento DOC',	'Entrada', '+'),
      TransactionOperation.new(9,'Aluguel',	'Saída', '-')
    ]

    def calculate(first_value, second_value, type)
      operation = from_type(type)
      first_value.public_send operation.operator, second_value
    end

    def from_type(type)
      KNOWN_OPERATIONS.find { |operation| operation.id == type }
    end
    
  end
end