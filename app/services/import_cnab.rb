class ImportCNAB
  def initialize(parser)
    self.parser = parser
  end

  def call
    import = Import.create(name: SecureRandom.uuid).tap do |import|
      parser.parse.map do |transaction|
        insert(transaction, import)
      end
    end
    import.save!
    import.transactions.map(&:store_id).uniq.map { |store_id| CNABImportWorker.perform_async(store_id, import.id) }
  end

  private

  def insert(data, import)
    Transaction.new.tap do |transaction|
      transaction.transaction_type = data.type
      transaction.date= data.date
      transaction.hour= data.hour
      transaction.card_number= data.card_number
      transaction.document= data.document
      transaction.amount= data.amount
      transaction.store = Store.find_or_create_by(name: data.store_name, owner: data.store_name)
      transaction.import = import
    end.save
  end

  attr_accessor :parser
end