class UpdateStoreCredits
  def initialize(store_id, import_id)
    self.store = Store.find(store_id)
    self.import = Import.find(import_id)
  end

  def call
    import.transactions.where(store: store).each do |transaction|
      store.current_balance = TransactionOperationConverter.calculate(store.current_balance, transaction.amount, transaction.transaction_type)
    end
    store.save
  end

  private

  attr_accessor :store, :import
end