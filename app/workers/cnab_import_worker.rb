class CNABImportWorker
  include Sidekiq::Worker

  def perform(store_id, import_id)
    UpdateStoreCredits.new(store_id, import_id).call
  end
end