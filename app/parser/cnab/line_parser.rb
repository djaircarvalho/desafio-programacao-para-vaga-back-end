module CNAB
  ParsedData = Struct.new(:type, :date, :amount, :document, :card_number, :hour, :store_owner, :store_name)

  class LineParser
   
    def initialize(data)
      self.data = data
    end

    def process
      ParsedData.new(parse_type, parse_date, parse_amount, parse_document, parse_card_number, parse_hour, parse_store_owner, parse_store_name) 
    end

    private
    
    TYPE_INTERVAL = 0
    DATE_INTERVAL = 1..8
    AMOUNT_INTERVAL = 9..18
    DOCUMENT_INTERVAL = 19..29
    CARD_NUMBER_INTERVAL = 30..41
    HOUR_INTERVAL = 42..47
    STORE_OWNER_INTERVAL = 48..61
    STORE_NAME_INTERVAL = 62..80

    def parse_type
      data[TYPE_INTERVAL].to_i
    end

    def parse_date
      data[DATE_INTERVAL]
    end

    def parse_amount
      data[AMOUNT_INTERVAL].to_i / 100.0
    end

    def parse_document
      data[DOCUMENT_INTERVAL]
    end

    def parse_card_number
      data[CARD_NUMBER_INTERVAL]
    end

    def parse_hour
      data[HOUR_INTERVAL]
    end

    def parse_store_owner
      data[STORE_OWNER_INTERVAL]
    end

    def parse_store_name
      data[STORE_NAME_INTERVAL]
    end

    attr_accessor :data
  end
end