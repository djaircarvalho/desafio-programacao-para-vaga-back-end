class CNABParser
  def initialize(file_path)
    self.file_path = file_path
  end

  def parse
    parse_lines
  end

  private

  def parse_lines
    File.readlines(file_path).map do |line|
      CNAB::LineParser.new(line).process()
    end
  end

  private

  attr_accessor :file_path
end
