class StoresController < ApplicationController
  def index
    @stores = Store.order(:id)
  end

  def show
    @store = Store.find(params[:id])
    @transactions = Transaction.where(store: @store)
  end
end
