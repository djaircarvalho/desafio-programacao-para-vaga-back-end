class ImportsController < ApplicationController
  def index
    @imports = Import.all
  end

  def new
  end

  def create
    ImportCNAB.new(CNABParser.new(params[:file].path)).call
    redirect_to :imports
  end
end
