class Transaction < ApplicationRecord
  belongs_to :store
  belongs_to :import

  validates_presence_of :hour, :date, :card_number, :transaction_type, :amount, :document

  def parsed_date
    Time.parse("#{date}#{hour}")
  end

end
