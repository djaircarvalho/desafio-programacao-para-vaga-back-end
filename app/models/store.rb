class Store < ApplicationRecord
  validates_presence_of :name, :owner
end
